const _ = require('lodash');
const mongoose = require('mongoose');
const CV = mongoose.model('CV');
const { handleError, handleSuccess } = require('../../config/response');
const helpers = require('../helpers/helpers');
const multipleUploadMiddleware = require("../helpers/multipleUploadMiddleware");
const pdf = require('html-pdf');
const base64Arraybuffer = require("base64-arraybuffer");
const pdfTemplate = require('../helpers/htmlCV');
// đăng bán sản phẩm
exports.create = async (req, res) => {
  try {
    await multipleUploadMiddleware(req, res);
    if (!req.file) {
      throw new Error('Bạn phải chọn ít nhất 1 ảnh');
    }
    const user = req.user;
    let { cv_info, cv_target, cv_hooby, cv_edu, cv_exp, cv_skills } = req.body;
    cv_info = JSON.parse(cv_info);
    cv_info["cv_avatar"] = req.file.filename;
    const cv = new CV({
      "cv_info": cv_info,
      "cv_target": JSON.parse(cv_target),
      "cv_hooby": JSON.parse(cv_hooby),
      "cv_edu": JSON.parse(cv_edu),
      "cv_exp": JSON.parse(cv_exp),
      "cv_skills": JSON.parse(cv_skills),
      "creater": helpers.getObjectId(user),
    });
    await cv.save();
    pdf.create(pdfTemplate(req.body), { "renderDelay": 5000 }).toBuffer(function (err, buffer) {
      if (err) {
        console.log(err);
      }
      const data = base64Arraybuffer.encode(buffer);
      return handleSuccess(res, 200, data, "Success");
    });
  } catch (error) {
    if (error.code === "LIMIT_UNEXPECTED_FILE") {
      return handleError(res, 400, `Exceeds the number of files allowed to upload.`);
    }
    return handleError(res, 400, error.message);
  }
};