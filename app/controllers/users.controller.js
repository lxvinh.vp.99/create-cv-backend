const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const jwt = require('jsonwebtoken');
const { handleError, handleSuccess } = require('../../config/response');
const config = require('../../config/env/all');
const helpers = require('../helpers/helpers');
const uuid = require('uuid');

// register
exports.create = async (req, res) => {
    const { confirmPassword, email, password, firstName, lastName } = req.body;
    delete req.body.resetPasswordToken;
    delete req.body.resetPasswordExpires;
    if (!email || !password || !confirmPassword || !lastName || !firstName) {
        return handleError(res, 400, 'Vui lòng điền đầy đủ thông tin');
    };
    const user = new User({
        ...req.body
    });
    const hasExistUser = await User.findOne({ email: user.email });
    if (hasExistUser) {
        return handleError(res, 400, 'Tên tài khoản đã được sử dụng!');
    }
    if (user.password !== confirmPassword) {
        return handleError(res, 400, 'Mật khẩu không trùng khớp');
    }
    await user.hashPassword();
    try {
        await user.save();
        return handleSuccess(res, 201, null, 'Đăng ký thành công');
    } catch (error) {
        console.log(error);
        return handleError(res, 400, _.last(error.message.split(':')).trim());
    }
}

// login
exports.login = async function (req, res) {
    const { email, password } = req.body;
    if (!email || !password) {
        return handleError(res, 400, 'Vui lòng điền đầy đủ thông tin');
    }
    const user = await User.findOne({ email }).exec();
    if (!user) {
        return handleError(res, 400, 'Tài khoản không tồn tại!');
    }
    const comparePassword = user.authenticate(password);
    if (!comparePassword) {
        return handleError(res, 400, 'Mật khẩu không chính xác!');
    }
    var payload = { email: user.email };
    var token = jwt.sign(payload, config.SECRETKEY, { expiresIn: '1h' });
    handleSuccess(res, 200, { token }, 'Đăng nhập thành công');
};

// my profile
exports.profile = async function (req, res) {
    let user = {
        // _id: req.user._id,
        email: req.user.email,
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        phoneNumber: req.user.phoneNumber,
        address: req.user.address,
    };
    handleSuccess(res, 200, user, 'Thành công');
};

// change password
exports.changePassword = async (req, res) => {
    try {
        const reqUser = req.user;
        const { oldPassword, newPassword } = req.body;
        if (oldPassword == null || newPassword == null) {
            throw new Error('Vui lòng điền đầy đủ thông tin');
        }
        const user = await User.findOne({
            email: reqUser.email,
        });
        if (!user) {
            throw new Error('Không tìm thấy tài khoản hoặc token hết hạn. Vui lòng đăng nhập lại');
        }
        if (!user.authenticate(oldPassword)) {
            throw new Error('Mật khẩu cũ không chính xác');
        }
        user.password = newPassword;
        await user.hashPassword();
        await user.save();
        handleSuccess(res, 201, { message: 'Thay đổi mật khẩu thành công' });
    } catch (error) {
        handleError(res, 401, error.message);
    }
}

// edit profile
exports.editprofile = async (req, res) => {
    try {
        const reqUser = req.user;
        if (!req.body.firstName || !req.body.lastName) {
            throw new Error('Vui lòng không để trống họ tên');
        }
        let user = await User.findOne({ email: reqUser.email });
        if (!user) {
            throw new Error('Không tìm thấy tài khoản hoặc token hết hạn. Vui lòng đăng nhập lại');
        }
        delete req.body.email;
        delete req.body.password;
        delete req.body.resetPasswordToken;
        delete req.body.resetPasswordExpires;
        user = _.assignIn(user, req.body);
        await user.save();
        handleSuccess(res, 201, null, 'Cập nhật thành công');
    } catch (error) {
        console.log(error.message);
        handleError(res, 400, error.message);
    }

}

// forgot password 
exports.forgotpassword = async (req, res) => {
    try {
        const { email } = req.body;
        if (!email) {
            return handleError(res, 401, 'Chưa nhập vào username');
        }
        const user = await User.findOne({
            email: email,
        });
        if (!user) {
            return handleError(res, 401, 'Không tồn tại tài khoản');
        }
        const tokenReset = uuid.v1();
        user.resetPasswordToken = tokenReset;
        user.resetPasswordExpires = Date.now() + 3600000; // trong 1h
        await helpers.sendMailResetPass(user.email, tokenReset);
        await user.save();
        return handleSuccess(res, 200, null, 'Vui lòng kiểm tra email để thay đổi mật khẩu');
    } catch (error) {
        handleError(res, 401, error.message);
    }
}
// verify token reset 
exports.verify = async (req, res) => {
    try {
        handleSuccess(res, 200, { email: req.user.email }, 'Xác thực thành công');
    } catch (error) {
        handleError(res, 401, error.message);
    }
}
// reset password
exports.resetpassword = async (req, res) => {
    try {
        let { user } = req;
        let { password, confirmPassword } = req.body;
        if (!password || !confirmPassword) {
            throw new Error('Vui lòng điền đầy đủ thông tin');
        }
        if (password !== confirmPassword) {
            throw new Error('Mật khẩu không trùng khớp');
        }
        user.password = password;
        user.resetPasswordToken = null;
        user.resetPasswordExpires = null;
        await user.hashPassword();
        await user.save();
        // await sendMailSuccessResetPass(user.email);
        handleSuccess(res, 200, '', 'Đặt lại mật khẩu thành công');
    } catch (error) {
        handleError(res, 400, error.message);
    }
}
_.assignIn(module.exports, require('./authenticate.controller'));
