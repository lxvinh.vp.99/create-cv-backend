const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CVSchema = new Schema(
  {
    "cv_info": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "cv_target": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "cv_hooby": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "cv_edu": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "cv_exp": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "cv_skills": {
      type: String,
      get: function (data) {
        JSON.parse(data);
      },
      set: function (data) {
        return JSON.stringify(data);
      }
    },
    "creater": {
      type: Schema.ObjectId,
      ref: 'User',
    }
  },
  {
    usePushEach: true,
    timestamps: true,
  }
);
mongoose.model('CV', CVSchema);
