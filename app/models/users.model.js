const bcryptjs = require('bcryptjs');
// const _ = require('lodash');
const mongoose = require('mongoose');
const helpers = require('../helpers/helpers');
const Schema = mongoose.Schema;
const userSchema = new Schema(
    {
        email: {
            type: String,
            match: /^\S+@\S+\.\S+$/,
            trim: true,
            lowercase: true
        },
        firstName: { type: String, required: [true, 'Chưa nhập first name'] },
        lastName: { type: String, required: [true, 'Chưa nhập last name'] },
        password: {
            type: String,
            required: true,
            default: '123456789',
        },
        phoneNumber: {
            type: String,
            match: /\d+/,
        },
        address: {
            type: String
        },
        resetPasswordToken: {
            type: String
        },
        resetPasswordExpires: {
            type: Date
        }
    },
    {
        usePushEach: true,
        timestamps: true,
    }
)

// userSchema.post('save', async function (doc, next) {
//     const WishList = mongoose.model('WishList');
//     const wishList = await WishList.findOne({ user: helpers.getObjectId(doc) });
//     if (!wishList) {
//         const newWishList = new WishList({
//             user: helpers.getObjectId(doc),
//             products: [],
//         });
//         await newWishList.save();
//     }
//     next();
// })

// userSchema.path('email').set(function(email) {
//   if (!this.picture && this.picture.indexOf('https://gravatar.com') === 0) {
//     const hash = crypto
//       .createHash('md5')
//       .update(email)
//       .digest('hex')
//     this.picture = `https://gravatar.com/avatar/${hash}?d=identicon`
//   }

//   if (!this.name) {
//     this.name = email.replace(/^(.+)@.+$/, '$1')
//   }
//   return email
// })

// userSchema.pre('save', async function (next) {
//     next()
// })

// userSchema.post('save', async function (doc, next) {
//     const WishList = mongoose.model('WishList')
//     const wishlist = await WishList.findOne({ user: helper.getObjectId(doc) })
//     if (!wishlist) {
//         const newWishList = new WishList({
//             user: helper.getObjectId(doc),
//             trips: [],
//         })
//         await newWishList.save()
//     }
//     if (!doc.userID) {
//         const IdentityCounter = mongoose.model('IdentityCounter')
//         const counter = await IdentityCounter.findOne({
//             model: 'User',
//             field: 'userID',
//         })
//         await IdentityCounter.findOneAndUpdate(
//             { model: 'User', field: 'userID' },
//             { $inc: { count: 1 } },
//             { upsert: true, new: true }
//         )
//         await User.findOneAndUpdate(
//             { _id: helper.getObjectId(doc) },
//             { $set: { userId: _.get(counter, 'count', 0) } }
//         )
//     }
//     next()
// })

userSchema.methods.hashPassword = async function () {
    if (!this.password) {
        throw new Error('Chưa nhập mật khẩu');
    } else if (this.password.length < 6 || this.password.length > 20) {
        throw new Error('Mật khẩu phải có độ dài từ 6 đến 20 kí tự');
    } else {
        let salt = bcryptjs.genSaltSync(10);
        this.password = bcryptjs.hashSync(this.password, salt);
    }
}

userSchema.methods.authenticate = function (password) {
    return bcryptjs.compareSync(password, this.password)
}

const User = mongoose.model('User', userSchema);
module.exports = User;
