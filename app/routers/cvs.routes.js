const cv = require('../controllers/cvs.controller');
const user = require('../controllers/users.controller');
module.exports = app => {
  app.route('/cvs/create').post(user.isAuthenticated, cv.create);
}
