require('dotenv').config();
const _ = require('lodash');
module.exports = _.assignIn(
  {
    ENV: process.env.NODE_ENV || "development",
    PORT: process.env.PORT || 3000,
    SECRETKEY: process.env.SECRETKEY,
    HOST: process.env.HOST || `http://localhost:${process.env.PORT}`,
    // HOST: process.env.HOST || `http://192.168.100.102:${process.env.PORT}`,
    FRONT_HOST : process.env.FRONT_HOST
  },
  require('./development') || {}
)
